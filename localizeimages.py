#!/usr/bin/env python3
from __future__ import print_function
import html5lib
import argparse, sys, os, re, json
from urllib.parse import urljoin, urlparse, quote as urlquote, unquote as urlunquote
from urllib.request import urlopen, Request
from urllib.error import URLError, HTTPError
import uuid, hashlib
from xml.etree import ElementTree as ET
from PIL import Image


def image_extension (url):
    ext = os.path.splitext(urlunquote(urlparse(url).path))[1].lower()
    if ext not in (".jpg", ".jpeg", ".png", ".gif"):
        try:
            f = urlopen(url)
            ct = f.info().get("content-type", "").lower()
            if ct == "image/png":
                ext = ".png"
            elif ct == "image/gif":
                ext = ".gif"
            else:
                ext = ".jpg"
        except URLError as e:
            ext = ""
        except HTTPError as e:
            ext = ""
        except ValueError as e:
            ext = ""
    return ext

def local_image_filename_for_url (url, path=''):
    """ Use the base name from url path + 6 chars of md5 of the URL itself for some sense of determined uniqueness """
    base = os.path.splitext(os.path.basename(urlunquote(urlparse(url).path)))[0]
    h = hashlib.md5()
    h.update(url.encode("utf-8"))
    return os.path.join(path, base+"_"+h.hexdigest()[:6] + image_extension(url))

def wget (url, topath, blocksize=4*1000, user_agent='Mozilla/5.0'):
    # if type(url) == unicode:
    #     url = url.encode("utf-8")
    try:
        # fin = urlopen(url)
        fin = urlopen(Request(url, headers={'User-Agent': user_agent}))
        ct = fin.info().get("content-type")
        count = 0
        with open(topath, "wb") as fout:
            while True:
                data = fin.read(blocksize)
                if not data:
                    break
                fout.write(data)
                count += len(data)
        if count > 0:
            return topath

    except HTTPError as e:
        print ("[wget]: skipping {0} HTTP ERROR {1}".format(url, e.code), file=sys.stderr)
        return None
    except URLError as e:
        print ("[wget]: skipping {0} URL ERROR {1}".format(url, e.reason), file=sys.stderr)
        return None

########################
# Image Resizing
########################

def fitbox (boxw, boxh, w, h):
    rw = boxw
    rh = int(rw * (float(h) / w))
    if (rh >= boxh):
        rh = boxh
        rw = int(rh * (float(w) / h))
    return rw, rh

def ensure_image_pyramid (im, maxz=3, tilew=256, tileh=256, base="", template="{0[base]}_z{0[z]}.jpg", force=False):
    z = 0
    boxw, boxh = tilew, tileh
    output = []
    while True:
        rw, rh = fitbox(boxw, boxh, im.size[0], im.size[1])

        op = template.format({'z':z, 'base':base})
        if force or not os.path.exists(op):
            rim = im.resize((rw, rh), Image.ANTIALIAS)
            tim = Image.new("RGB", (rw, rh))
            tim.paste(rim, (0, 0))
            print ("saving {0}".format(op), file=sys.stderr)
            tim.save(op)

        output.append({'z': z, 'width': rw, 'height': rh, 'path': op})
        z += 1
        if z>maxz:
            break
        boxw *= 2
        boxh *= 2
        if rw > im.size[0]:
            # stop once we've exceeded the original
            break

    return output

def image_pyramid (path, include_original=False):
    im = Image.open(path)
    base = os.path.splitext(path)[0]
    d = ensure_image_pyramid(im, base=base)
    if include_original:
        d.append({"path": path, "width": im.size[0]})
    return d

def process(input, baseurl, imagespath, output):
    t = html5lib.parse(input, namespaceHTMLElements=False)
    for img in t.findall(".//img[@src]"):
        src = urljoin(baseurl, img.attrib.get("src"))
        newpath = local_image_filename_for_url(src, imagespath)
        if not os.path.exists(newpath):
            print ("Downloading image {0} to {1}".format(src, newpath), file=sys.stderr)
            newpath = wget(src, newpath)
        if newpath:
            ip = image_pyramid(newpath)
            # print (json.dumps(ip, indent=2), file=sys.stderr)
            if ip:
                img.attrib["src"] = urlquote(ip[0]['path'])
                img.attrib["srcset"] = ", ".join(["{0} {1}w".format(x['path'], x['width']) for x in ip[1:]])
            else:
                newsrc = urlquote(newpath)
                img.attrib["src"] = newsrc
    print(ET.tostring(t, method="html", encoding="unicode"), file=output)

if __name__ == "__main__":

    ap = argparse.ArgumentParser("")
    ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
    ap.add_argument("--baseurl", default="http://etherbox.local/etherdump/")
    ap.add_argument("--imagespath", default="images")
    ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
    args = ap.parse_args()
    try:
        os.makedirs(args.imagespath)
    except OSError:
        pass
    process(args.input, baseurl=args.baseurl, imagespath=args.imagespath, output=args.output)
